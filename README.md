# diogenes

A docker storage driver to manage a snapshot capable filesystem like ZFS with 
some tools to make your life easier.

## Why

As a developer I need a tool manage easily docker volumes in zfs because:
* I want get a fresh version of a big volume of data, apply changes over it with
  a myself developed program and if it fails restore previous version as fast
  as possible.
* I don't want duplicate initial version of data and version against I test my
  code. Snapshots its a requirements.
* I'm very happy using zfs but I want a easier way to use it with dockerizer
  environments
  * Each docker volume must be a zfs dataset
  * I want an easy way to migrate docker volumes to a zfs type volume
  * I want an easy way to associate a docker volume name to a zfs dataset
    without naming in docker with his pool and complete hierarchy
  * I want to be capable to expose a zfs volume as zfs and import it with
    nfs from other machine
  * I want an easy way to change between snapshots
  * I want an easy way to configure autosnapshots by date and their rotations
  * I want an easy way to copy or clone a volume snapshot to other volume


Integrated docker support for zfs is not sufficient to me. Other projects like
https://github.com/TrilliumIT/docker-zfs-plugin exists but force you to name
docker volumes like full dataset name in zfs.

## Caveats

Exists issues managing zfs inside a container so this volume could not be
possible to be installed as container. This make install harder and some
packages to do in a easy way should be created

Some features may can not been as `docker volume` subcommand and extra command
should be created.

## Plan

### MVP

Local volume plugin for zfs. ZFS dataset is calculated appending volume name to
default base dataset or base dataset passed as option or like volume tag. ZFS
dataset is stored as a created volume tag and used to mount correct dataset.

Debian packages to build ubuntu packages of volume plugin.

### Global features

Extend capabilities to global support and share volumes via NFS. Remote
machines will mount volumes with NFS and lacal machines with ZFS.

Add notification and monitorization configurable checks to generate alerts
when space has been exhausted, expose metrics from system (prometheus endpoint
for example).

Add configuration for perform automated tasks like run scrub or autogenerate
snapshots.

Add a command to list volume capabilities like compression, 
deduplication,... and manage it. This command must not be coupled to zfs
implementation. In a future more filesystems could be supported.

Add a API to plugin daemon to allow automate storage stuff (add disks to pools,
migrate volumes, force maintenance tasks,...)

### Better usability

Generate better error messages, write docs, check config,...

Build packages for more distros or OS.

### Launch

Stabilize and release v1. Spread the word and write blogs entries about hot to
use it and why you must use instead other volume plugins.

## Example use cases

### Development

#### Evolutionary Database Design

Jen is working in a migration in her own database environment like described
[here](https://martinfowler.com/articles/evodb.html). Devops teams has
developed a tool that do next things transparent for her:

* Once a day a full copy of production database is copied to mirror copy of
  production databse via send a recv zfs commands
* A docker image of postgresql pointing to that zfs volume will be running.
* After execute an anonimiztion tool over mirror database to
  enforce data privacy laws a snapshot is created
* Jen can exec a tool to request a fresh copy of her database. The tool use
  zfs clone command to create a zero copy dataset with database data in
  seconds using the anonimized snapshot. A docker image of postgresql pointing 
  to cloned dataset will be running.

Jen can made changes over her db instance or request new fresh copies when she
wants.

Other members of team uses same zero copied fresh images to work in theirs own 
database environments. Changes in their own environmentes are allowed and are
isolated from other team members.

Space is not a problem for devops teams because most data is shared between
instances. Only changes in each environment increases disk space used.
