module gitlab.com/paascual/diogenes

go 1.12

require (
	github.com/Microsoft/go-winio v0.4.14 // indirect
	github.com/coreos/go-systemd v0.0.0-20190719114852-fd7a80b32e1f // indirect
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-plugins-helpers v0.0.0-20181025120712-1e6269c305b8
	github.com/mistifyio/go-zfs v2.1.1+incompatible
	golang.org/x/net v0.0.0-20191003171128-d98b1b443823 // indirect
)
