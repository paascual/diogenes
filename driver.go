package diogenes

import (
	"errors"
	"log"
	"path"
	"strings"

	"github.com/docker/go-plugins-helpers/volume"
	"github.com/mistifyio/go-zfs"
)

var (
	// Check that driver implements all methods in volume.Driver interface.
	_ volume.Driver = &Driver{}

	// ErrNotImplemented is returned when the method is not implemented.
	ErrNotImplemented = errors.New("not implemented")
)

// Driver implements diogenes volume driver.
type Driver struct {
	pool string
}

// NewDriver creates a new diogenes volume driver.
func NewDriver(pool string) *Driver {
	return &Driver{
		pool: pool,
	}
}

func (d *Driver) Create(r *volume.CreateRequest) error {
	log.Printf("diogenes create: %s", r.Name)

	name := d.name(r.Name)
	log.Printf("new dataset name: %s", name)

	_, err := zfs.CreateFilesystem(name, nil)
	if err != nil {
		return err
	}

	return nil
}

func (d *Driver) List() (*volume.ListResponse, error) {
	log.Printf("diogenes list")

	dss, err := zfs.Datasets(d.pool)
	if err != nil {
		return nil, err
	}

	dsPrefix := d.pool + "/"

	var vols []*volume.Volume
	for _, ds := range dss {
		log.Printf("  ds: %s", ds.Name)
		if ds.Name == d.pool {
			continue
		}

		name := strings.TrimPrefix(ds.Name, dsPrefix)
		vols = append(vols, &volume.Volume{
			Name:       name,
			Mountpoint: ds.Mountpoint,
		})
	}

	list := &volume.ListResponse{
		Volumes: vols,
	}

	return list, nil
}

func (d *Driver) Get(r *volume.GetRequest) (*volume.GetResponse, error) {
	log.Printf("diogenes get: %s", r.Name)

	ds, err := zfs.GetDataset(d.name(r.Name))
	if err != nil {
		log.Printf("ERROR: %s", err.Error())
		return nil, err
	}

	log.Printf("mount point: %s", ds.Mountpoint)

	resp := &volume.GetResponse{
		Volume: &volume.Volume{
			Name:       r.Name,
			Mountpoint: ds.Mountpoint,
		},
	}

	return resp, nil
}

func (d *Driver) Remove(r *volume.RemoveRequest) error {
	log.Printf("diogenes remove: %s", r.Name)

	ds, err := zfs.GetDataset(d.name(r.Name))
	if err != nil {
		log.Printf("ERROR: %s", err.Error())
		return err
	}

	var flags zfs.DestroyFlag
	return ds.Destroy(flags)
}

func (d *Driver) Path(r *volume.PathRequest) (*volume.PathResponse, error) {
	log.Printf("diogenes path: %s", r.Name)

	return nil, ErrNotImplemented
}

func (d *Driver) Mount(r *volume.MountRequest) (*volume.MountResponse, error) {
	log.Printf("diogenes mount: %s", r.Name)

	ds, err := zfs.GetDataset(d.name(r.Name))
	if err != nil {
		log.Printf("ERROR: %s", err.Error())
		return nil, err
	}

	log.Printf("mount point: %s", ds.Mountpoint)

	resp := &volume.MountResponse{
		Mountpoint: ds.Mountpoint,
	}

	return resp, nil
}

func (d *Driver) Unmount(r *volume.UnmountRequest) error {
	log.Printf("diogenes umount: %s", r.Name)

	return ErrNotImplemented
}

func (d *Driver) Capabilities() *volume.CapabilitiesResponse {
	// copied from https://github.com/vieux/docker-volume-sshfs/blob/master/main.go
	return &volume.CapabilitiesResponse{
		Capabilities: volume.Capability{
			Scope: "local",
		},
	}
}

func (d *Driver) name(dataset string) string {
	return path.Join(d.pool, dataset)
}
