package main

import (
	"flag"
	"log"

	"github.com/docker/go-plugins-helpers/volume"
	"gitlab.com/paascual/diogenes"
)

const socketAddress = "/run/docker/plugins/diogenes.sock"

func main() {
	pool := flag.String("pool", "", "zfs pool")
	flag.Parse()

	driver := diogenes.NewDriver(*pool)
	handler := volume.NewHandler(driver)

	err := handler.ServeUnix(socketAddress, 0)
	if err != nil {
		log.Fatal("server error", err)
	}
}
